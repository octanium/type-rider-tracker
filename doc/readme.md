# TypeRiderTracker

_Documentation_

---

## Changelog

### v0.2.3
- Added fullscreen mode to tracker, press 'f' to toggle
- Fixed issue with % char preventing loading config file
- Cleaning

### v0.2.2
- Added minArea & maxArea parameters
- Added numPtsTriangulableMax parameter
- Cleaning & bugfixes

### v0.2.1
- Fixed bug preventing using high resolution (1280x960)
- Fixed bug preventing the camera from being properly disconnected
- Added keyboard control over quad warp points

### v0.2
- the GUI can be hidden to save resources by pressing TAB key
- Fixed bug that could crash the app when no triangle was found
- Added up-to-date documentation

### v0.1.1
- Added control to set number of triangles sent per OSC message
- Fixed bug preventing all triangles from being sent

---

## Presentation

_TypeRiderTracker is a set of 3 MacOSX applications_

#### • TypeRiderTracker
<img src="../res/icon.png" alt="Tracker Icon" style="width: 150px;"/>

App that uses an IR Libdc85-compliant camera to detect contrasted shapes, and send their trianlulated representations via OSC

#### • TypeRiderTracker Simulation
<img src="../res/icon_simu.png" alt="Tracker Icon" style="width: 150px;"/>

Same as tracker, but simulates the camera input with an image loaded from the data folder.

#### • OSCTriangleTest
<img src="../res/icon_test.png" alt="Tracker Icon" style="width: 150px;"/>

Application that receives and displays triangles sent via OSC. Usefull to check if the address/port/network communication is setup correctly.

---

## Installation

* All 3 applications require recent Mac computer running OSX 10.7 or later
* Extract the release zip on your system
* Make sure to keep the folder "data" and its content
* You can make aliases of the executables

---

## Usage

### Files :

```
├── data
│   ├── calibration.yml            <- camera's undistortion calibration file
│   ├── default_simulation.xml     <- settings for the simulation
│   ├── default.xml                <- settings for the tracker
│   ├── simulation.png             <- input image for the simulation
├── OSCTriangleTest                <- the test application
├── readme.pdf                     <- this file
├── TypeRiderTracker               <- the tracker
├── TypeRiderTracker Simulation    <- the simulation
```

### Settings :

Most settings are configurable through the GUI of the applications.

#### Quad Warp:

In order to make the camera input match the videoprojector output, the GUI provides 4 draggable points that have to be  positionned respectively on each corners of the projection.

Because the camera is only seeing infrareds, you have to use a physical marker or an IR LED and successively set each corner.

#### Application Settings :

| Name | default | description |
| ---- | ------- | ----------- |
| Framerate | 20 | framerate of the application. Lower values will free up resources for the front-end app. |
| VideoProj width | 1024 | Resolution X of the video projector |
| VideoProj height | 738 | Resolution Y of the video projector |
| Num triangles per message | 50 | Number of triangles sent per OSC message. Doesn't affect the number of triangles sent per update. |

#### Contours :

| Name | default | description |
| ---- | ------- | ----------- |
| Contours Threshold | ~175 | Threshold of the contour finder. Lower values will include brighter pixels in the contours, thus making them bigger. |
| Smooth | 2 | Smoothing factor applied to the contour finder. Higher values will reduce the total number of triangles but will reduce accuracy of shape representation |
| Smooth Shape | ~0.35 | Smooth factor applied to the contour finder that affects how the smooth should be performed |
| Simplification | ~0.30 | Factor that reduce the number of points of the contour finder's output. Higher values will greatly reduce the number of triangles but will decrease the accuracy. |
| Draw | x | Wether or not draw the contour finder results. Deactivate to save some resources. |

#### Triangle :

| Name | default | description |
| ---- | ------- | ----------- |
| Draw | x | Wether or not draw the triangulation results. Deactivate to save some resources. |
| Num Triangle | N/A | **Read-Only** label that tells how many triangles are being found and transmitted via OSC |

#### Application Settings :

The following settings aren't editable via the GUI, and must be configured in the `data/defaults.xml` for the tracker and `data/defaults_simulation.xml` for the simulation

| Name | type | default | description |
| ---- | ---- | ------- | ----------- |
| OSC Host | string | localhost | address OSC packets are sent to |
| OSC Port | int | 12345 | port OSC packets are sent on |

[1]: ../res/icon.png 