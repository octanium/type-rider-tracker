//
//  Utils.h
//  TypeRiderTracker
//
//  Created by kikko on 9/2/13.
//
//

#ifndef __TypeRiderTracker__Utils__
#define __TypeRiderTracker__Utils__

#include "ofMain.h"

namespace octanium {
    
    template <class SetterType, class ParamType>
    class SetterParameter : public ofParameter<ParamType> {
    public:
        void addSetter(SetterType * listener, void (SetterType::* setter)(float)){
            this->listener = listener;
            this->setter = setter;
            this->addListener(this, &SetterParameter::parameterChanged);
        }
    private:
        SetterType * listener;
        void (SetterType::* setter)(float);
        void parameterChanged(ParamType& value){
            mem_fun (setter)(listener, value);
        }
    };
    
    template <class ParamType>
    class GlobalSetterParameter : public ofParameter<ParamType> {
    public:
        void addSetter(void (* setter)(ParamType)){
            this->setter = setter;
            this->addListener(this, &GlobalSetterParameter::parameterChanged);
        }
    private:
        void (* setter)(ParamType);
        void parameterChanged(ParamType& value){
            setter(value);
        }
    };
}

#endif /* defined(__TypeRiderTracker__Utils__) */
