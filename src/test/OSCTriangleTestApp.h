#pragma once

#include "ofMain.h"
#include "ofxOsc.h"

class OSCTriangleTestApp : public ofBaseApp{

public:
    void setup();
    void update();
    void draw();
    
    void keyPressed(int key);
    
    ofxOscReceiver osc;
    ofMesh mesh;
    ofMesh nextMesh;
    bool bClear;
};
