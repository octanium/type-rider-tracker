
#include "OSCTriangleTestApp.h"

void OSCTriangleTestApp::setup(){
    ofSetVerticalSync(true);
    ofBackground(20);
    ofSetFrameRate(30);
    osc.setup(12345);
}

void OSCTriangleTestApp::update(){
    
    while(osc.hasWaitingMessages()){
		ofxOscMessage m;
		osc.getNextMessage(&m);
        if(m.getAddress() == "/t"){
            ofPoint v;
            int numTriangles = m.getNumArgs() / 6;
            for (int i=0; i<numTriangles; i++) {
                for (int j=0; j<3; j++) {
                    v.x = m.getArgAsInt32(i*6+j*2);
                    v.y = m.getArgAsInt32(i*6+j*2+1);
                    nextMesh.addVertex(v);
                    nextMesh.addIndex(nextMesh.getNumVertices()-1);
                }
            }
        } else if(m.getAddress() == "/end"){
            mesh = nextMesh;
            nextMesh.clear();
        }
    }
}

void OSCTriangleTestApp::draw(){
    mesh.drawWireframe();
    ofDrawBitmapString("Listening on port 12345 \nPress 'f' for fullscreen", 10, 20);
}

void OSCTriangleTestApp::keyPressed(int key){
    switch (key) {
        case 'f':
            ofToggleFullscreen();
            break;
    }
}

