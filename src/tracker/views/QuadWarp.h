//
//  QuadWarp.h
//  typerider
//
//  Created by kikko on 9/2/13.
//
//

#ifndef __typerider__QuadWarp__
#define __typerider__QuadWarp__

#include "ofMain.h"
#include "ofxMSAInteractiveObject.h"
#include "DataTypes.h"

namespace octanium {
    
    class QuadWarpPoint : public ofxMSAInteractiveObject {
    public:
        virtual ~QuadWarpPoint(){
        }
        
        bool hasChanged;
        
        void setup();
        void draw();
        
        void onPress(int x, int y, int button);
        void onDragOver(int x, int y, int button);
        void onDragOutside(int x, int y, int button);
        
        ofEvent<QuadWarpPoint> selected;
        
        bool hitTest(int tx, int ty){
            return ofPoint(tx - offset.x, ty - offset.y).distance(ofPoint(x, y)) < width + 2;
        }
    protected:
        ofPoint offset;
    };
    
    
    class QuadWarp {
    public:
        
        QuadWarp():focus(NULL){}
        
        void setup(const QuadVO& quad);
        void update();
        void draw();
        
        void set(const QuadVO& quad);
        
        ofEvent<QuadVO> hasChanged;
        
    private:
        
        void keyDown(ofKeyEventArgs& args);
        void ptSelected(QuadWarpPoint& pt);
        
        QuadWarpPoint* focus;
        QuadWarpPoint quad[4];
    };
}

#endif /* defined(__typerider__QuadWarp__) */
