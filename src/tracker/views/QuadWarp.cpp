//
//  QuadWarp.cpp
//  typerider
//
//  Created by kikko on 9/2/13.
//
//

#include "QuadWarp.h"

namespace octanium {

#pragma mark - QuadWarpPoint
    
    void QuadWarpPoint::setup(){
        hasChanged = true;
        enableMouseEvents();
        disableAppEvents();
    }
    
    void QuadWarpPoint::draw() {
        ofPushStyle();
        
        ofEnableAlphaBlending();
        
        if(isMousePressed()){
            ofSetColor(255, 255, 255, 5);
        }
        else if(isMouseOver()){
            ofSetColor(ofColor::white);
        }
        else{
            ofSetColor(255, 255, 255, 40);
        }
        
        //offset.x = (ofGetWidth()-CAM_WIDTH)*0.5;
        //offset.y = (ofGetHeight()-CAM_HEIGHT)*0.5;
        
        ofPushMatrix();
        ofTranslate(offset.x, offset.y);
        ofCircle(x, y, width);
        ofPopMatrix();
        
        ofDisableAlphaBlending();
        
        ofPopStyle();
    }
    
    void QuadWarpPoint::onPress(int x, int y, int button){
        selected.notify(this, *this);
    }
    
    void QuadWarpPoint::onDragOver(int x, int y, int button){
        x -= offset.x;
        y -= offset.y;
        position.set(x - width * 0.5, y - height * 0.5);
        hasChanged = true;
    }

    void QuadWarpPoint::onDragOutside(int x, int y, int button){
        x -= offset.x;
        y -= offset.y;
        position.set(x - width * 0.5, y - height * 0.5);
        hasChanged = true;
    }
    
#pragma mark - QuadWarp
    
    void QuadWarp::setup(const QuadVO& quadVO){
        for(int i = 0; i < 4; i++){
            quad[i].setup();
            ofAddListener(quad[i].selected, this, &QuadWarp::ptSelected);
        }
        ofAddListener(ofEvents().keyPressed, this, &QuadWarp::keyDown);
        set(quadVO);
    }
    
    void QuadWarp::set(const QuadVO &vo){
        int w = 10;
        quad[0].set(vo.pts[0].x, vo.pts[0].y, w, w);
        quad[1].set(vo.pts[1].x, vo.pts[1].y, w, w);
        quad[2].set(vo.pts[2].x, vo.pts[2].y, w, w);
        quad[3].set(vo.pts[3].x, vo.pts[3].y, w, w);
    }
    
    void QuadWarp::ptSelected(QuadWarpPoint& pt){
        focus = &pt;
    }
    
    void QuadWarp::keyDown(ofKeyEventArgs& args){
        if(!focus) return;
        
        switch (args.key) {
            case OF_KEY_UP: focus->position.y--; break;
            case OF_KEY_DOWN: focus->position.y++; break;
            case OF_KEY_RIGHT: focus->position.x++; break;
            case OF_KEY_LEFT: focus->position.x--; break;
            default: break;
        }
    }
    
    void QuadWarp::update(){
        bool bHasChanged = false;
        for(int i = 0; i < 4; i++){
            if(quad[i].hasChanged){
                bHasChanged = true;
                break;
            }
        }
        if(bHasChanged) {
            QuadVO vo;
            for(int i = 0; i < 4; i++){
                vo.pts[i] = quad[i].position;
            }
            ofNotifyEvent(hasChanged, vo);
        }
    }
    
    void QuadWarp::draw(){
        ofPushStyle();
        ofEnableAlphaBlending();
        ofSetColor(ofColor::pink);
        int i = 0;
        for(i = 0; i < 3; i++){
            ofLine(quad[i].x, quad[i].y, quad[i + 1].x, quad[i + 1].y);
            quad[i].draw();
        }
        ofLine(quad[i].x, quad[i].y, quad[0].x, quad[0].y);
        quad[i].draw();
        
        ofDisableAlphaBlending();
        ofPopStyle();
    }
    
}