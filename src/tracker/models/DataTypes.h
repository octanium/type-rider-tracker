//
//  DataTypes.h
//  TypeRiderTracker
//
//  Created by kikko on 9/2/13.
//
//

#ifndef TypeRiderTracker_DataTypes_h
#define TypeRiderTracker_DataTypes_h

#include "ofMain.h"

namespace octanium {
    
    class QuadVO {
    public:
        ofPoint pts[4];
        
        friend ostream& operator<<(ostream& os, const QuadVO& quad);
        friend istream& operator>>(istream& is, QuadVO& quad);
    };
    
    inline bool operator==(const QuadVO& lhs, const QuadVO& rhs){
        for(int i=0; i<4; i++){
            if(lhs.pts[i].distanceSquared(rhs.pts[i]) > 0.1){
                return false;
            }
        }
        return true;
    }
    inline bool operator!=(const QuadVO& lhs, const QuadVO& rhs){return !operator==(lhs,rhs);}
    
    inline ostream& operator<<(ostream& os, const QuadVO& quad) {
        os << quad.pts[0] << "; " << quad.pts[1] << "; " << quad.pts[2]  << "; " << quad.pts[3];
        return os;
    }
    
    inline istream& operator>>(istream& is, QuadVO& quad) {
        for(int i=0;i<4;i++){
            is >> quad.pts[i];
            is.ignore(2);
        }
        return is;
    }
    
    
    class ContoursVO {
    public:
        vector< ofPolyline > polylines;
        int numTotalPoints;
    };
    

    class TrianglesVO {
    public:
        vector< vector<ofVec2f> > triangles;
    };
}


#endif
