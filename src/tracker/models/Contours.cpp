//
//  Contours.cpp
//  typerider
//
//  Created by kikko on 9/2/13.
//
//

#include "Contours.h"

namespace octanium {

    void Contours::setup(){
        
        params.setName("Contours");
        threshold.set("Contours Threshold", 85, 0, 255);
        threshold.addSetter(&contourFinder, &ofxCv::ContourFinder::setThreshold);
        params.add( threshold );
        params.add(smooth.set("Smooth", 1, 1, 20));
        params.add(smoothShape.set("Smooth Shape", 0.0, 0, 1.0));
        params.add(simplification.set("Simplification", 0.1, 0.1, 3.0));
        params.add(minArea.set("Min area percent", 0.5, 0.01, 5));
        params.add(maxArea.set("Max area percent", 8, 0, 25));
        params.add(bDraw.set("Draw", false));
        report.set("Num Contours", "");
        
        //contourFinder.setFindHoles(true);
    }

    const ContoursVO & Contours::find(ofImage & frame, const QuadVO& quad, int outputWidth, int outputHeight){
        
        bool bUpdateQuadMask = (currQuad != quad);
        
        if(bUpdateQuadMask || outputWidth != currOutputWidth || outputHeight != currOutputHeight) {
            updateQuadHomography(quad, outputWidth, outputHeight);
            currQuad = quad;
        }
        
        cv::Mat masked = getQuadMasked(frame, quad, bUpdateQuadMask);
        
        contourFinder.setMinAreaNorm(minArea*0.01);
        contourFinder.setMaxAreaNorm(maxArea*0.01);
        
        contourFinder.findContours( masked );
        
        vo.polylines = contourFinder.getPolylines();
        
        unwarp();
        
        vo.numTotalPoints = 0;
        for (int i=0; i<vo.polylines.size();i++) {
            vo.polylines[i] = vo.polylines[i].getSmoothed(smooth, smoothShape);
            vo.polylines[i].simplify(simplification);
            vo.numTotalPoints += vo.polylines[i].size();
        }
        
        report.set( ofToString(vo.numTotalPoints) );
        
        return vo;
    }
    
    bool isPolylineInsideQuad(const QuadVO& quad, const ofPolyline& poly){
        
        
    }
    
    void Contours::updateQuadHomography(const QuadVO& quad, int outputWidth, int outputHeight){
        
        vector <cv::Point2f> srcPoints, dstPoints;
        for(int i = 0; i < 4; i++){
            srcPoints.push_back(cv::Point2f(quad.pts[i].x, quad.pts[i].y));
        }
        dstPoints.push_back(cv::Point2f(0, 0));
        dstPoints.push_back(cv::Point2f(outputWidth, 0));
        dstPoints.push_back(cv::Point2f(outputWidth, outputHeight));
        dstPoints.push_back(cv::Point2f(0, outputHeight));
        
        // generate a homography from the two sets of points
        homography = cv::findHomography(cv::Mat(srcPoints), cv::Mat(dstPoints));
        
        currOutputWidth = outputWidth;
        currOutputHeight = outputHeight;
    }
    
    cv::Mat Contours::getQuadMasked(ofImage &frame, const QuadVO& quad, bool bUpdateMask){
        
        cv::Mat cvFrame = ofxCv::toCv(frame);
        ofxCv::copy(cvFrame, frameMat);
        
        if(bUpdateMask || quadMask.empty()){
            ofxCv::imitate(quadMask, frameMat);
            quadMask.setTo(0.f);
            cv::Point pts[1][4];
            for (int i=0; i<4; i++) {
                pts[0][i] = cv::Point(quad.pts[i].x, quad.pts[i].y);
            }
            const cv::Point * ppt[1] = { pts[0] };
            int npt[] = { 4 };
            cv::fillPoly(quadMask, ppt, npt, 1, cv::Scalar(255,255,255));
        }
        ofxCv::invert(frameMat);
        frameMat = quadMask & frameMat;
        
        return frameMat;
    }
    
    void Contours::unwarp(){
        
        for (auto& pl: vo.polylines) {
            int size = pl.size();

            std::vector <cv::Point2f> trackPts_cv(size);
            std::vector <cv::Point2f> trackPts_cv_unwarped(size);
            
            int i;
            for(i = 0; i < size; i++){
                trackPts_cv[i].x = pl[i].x;
                trackPts_cv[i].y = pl[i].y;
            }
            perspectiveTransform(trackPts_cv, trackPts_cv_unwarped, homography);
            
            for(i = 0; i < size; i++){
                pl[i].x = trackPts_cv_unwarped[i].x;
                pl[i].y = trackPts_cv_unwarped[i].y;
            }
        }
    }
    
    void Contours::draw(){
        
        if(!bDraw) return;
        
        ofPushStyle();
        
        ofSetColor(ofColor::red);
        contourFinder.draw();
        
        ofPopStyle();
    }

}