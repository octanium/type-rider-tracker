//
//  Contours.h
//  typerider
//
//  Created by kikko on 9/2/13.
//
//

#ifndef __typerider__Contours__
#define __typerider__Contours__

#include "ofMain.h"
#include "DataTypes.h"
#include "Utils.h"
#include "ofxCv.h"

namespace octanium {
    
    class Contours {
        
    public:
        Contours(){}
        virtual ~Contours(){}
        
        void setup();
        const ContoursVO & find(ofImage & frame, const QuadVO& quad, int outputWidth, int outputHeight);
        void draw();
        
        ofParameterGroup params;
        SetterParameter<ofxCv::ContourFinder, float> threshold;
        ofParameter<float> simplification;
        ofParameter<int> smooth;
        ofParameter<float> smoothShape;
        ofParameter<bool> bDraw;
        ofParameter<float> minArea;
        ofParameter<float> maxArea;
        ofParameter<string> report;
        
    private:
        ContoursVO vo;
        QuadVO currQuad;
        ofxCv::ContourFinder contourFinder;
        cv::Mat homography;
        cv::Mat quadMask;
        cv::Mat frameMat;
        
        cv::Mat getQuadMasked(ofImage &frame, const QuadVO& quad, bool bUpdateMask);
        void updateQuadHomography(const QuadVO& quad, int outputWidth, int outputHeight);
        void unwarp();
        bool isPolylineInsideQuad(const QuadVO& quad, const ofPolyline& poly);
        
        int currOutputWidth, currOutputHeight;
    };
}

#endif /* defined(__typerider__Contours__) */
