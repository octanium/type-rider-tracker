//
//  SimulationCamera.cpp
//  typerider
//
//  Created by kikko on 8/29/13.
//
//

#include "SimulationCamera.h"

namespace octanium {

    void SimulationCamera::setup(){
        
        params.setName("Simulation");
        img.loadImage("simulation.png");
    }

    bool SimulationCamera::update(){
        
        return true;
    }

    ofParameterGroup & SimulationCamera::getParameters(){
        return params;
    }

    ofImage & SimulationCamera::getCurrFrame(){
        return img;
    }

}