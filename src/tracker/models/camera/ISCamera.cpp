//
//  ISCamera.cpp
//  typerider
//
//  Created by kikko on 8/29/13.
//
//

#include "ISCamera.h"

namespace octanium{

    void ISCamera::setup(){
        
        bool bFirewire800 = true;
        if(bFirewire800){
            camera.setFormat7(true);
            camera.set1394b(true);
        }
        
        camera.setImageType(OF_IMAGE_GRAYSCALE);
        camera.setSize(1280, 960);
        camera.setFrameRate(30);
        camera.setBlocking(false);
        
        if (ofxLibdc::Camera::getCameraCount() > 0) {
            bool result = camera.setup();
            curFrame.allocate(camera.getWidth(), camera.getHeight(), OF_IMAGE_GRAYSCALE);
        } else {
            curFrame.allocate(640, 480, OF_IMAGE_GRAYSCALE);
        }
        curFrame.setColor(ofColor::black);
        curFrame.update();
        
        setupParameters();
        
        calibration.setFillFrame(true); // true by default
        calibration.load("calibration.yml");
    }
    
    void ISCamera::setupParameters(){
        
        parameters.setName("ISCamera");
        
        brightness.set("Brightness", 0.f, 0, 1);
        brightness.addSetter(&camera, &ofxLibdc::Camera::setBrightness);
        parameters.add(brightness);
        
        exposure.set("Exposure", 0.f, 0, 0.1);
        exposure.addSetter(&camera, &ofxLibdc::Camera::setExposure);
        parameters.add(exposure);
        
        gain.set("Gain", 0.f, 0, 1);
        gain.addSetter(&camera, &ofxLibdc::Camera::setGain);
        parameters.add(gain);
        
        shutter.set("Shutter", 0.2, 0, 0.05);
        shutter.addSetter(&camera, &ofxLibdc::Camera::setShutter);
        parameters.add(shutter);
        
        gamma.set("Gamma", 0.f, 0, 1);
        gamma.addSetter(&camera, &ofxLibdc::Camera::setGamma);
        parameters.add(gamma);
        
        parameters.add(bUndistord.set("Undistord", true));
    }
    
    ofImage& ISCamera::getCurrFrame(){
        return curFrame;
    }

    bool ISCamera::update(){
        
        if(camera.grabVideo(curFrame)){
            if(bUndistord){
                calibration.undistort(ofxCv::toCv(curFrame));
            }
            curFrame.update();
            return true;
        }
        return false;
    }
    
}
