//
//  SimulationCamera.h
//  typerider
//
//  Created by kikko on 8/29/13.
//
//

#ifndef __typerider__SimulationCamera__
#define __typerider__SimulationCamera__

#include "ofMain.h"
#include "ICamera.h"

namespace octanium {
    
    class SimulationCamera : public ICamera {
        
        void setup();
        bool update();
        bool isReady() { return true; }
        
        ofParameterGroup & getParameters();
        
        ofImage & getCurrFrame();
        
    private:
        ofParameterGroup params;
        ofImage img;
    };
    
}

#endif /* defined(__typerider__SimulationCamera__) */
