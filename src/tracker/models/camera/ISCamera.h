//
//  ISCamera.h
//  typerider
//
//  Created by kikko on 8/29/13.
//
//

#ifndef __typerider__ISCamera__
#define __typerider__ISCamera__

#include "ofMain.h"
#include "ICamera.h"
#include "ofxLibdc.h"
#include "ofxCv.h"
#include "Utils.h"

namespace octanium {
    
    class ISCamera : public ICamera {
        
    public:
        
        void setup();
        bool update();
        bool isReady() { return camera.isReady(); }
        
        ofParameterGroup parameters;
        SetterParameter<ofxLibdc::Camera, float> brightness, shutter, gain, gamma, exposure;
        ofParameter<bool> bUndistord;
        
        ofParameterGroup & getParameters() {
            return parameters;
        }
        
        ofImage & getCurrFrame();
        
    private:
        
        ofxLibdc::Camera camera;
        ofImage curFrame;
        ofxCv::Calibration calibration;
        
        void setupParameters();
    };
}

#endif /* defined(__typerider__ISCamera__) */
