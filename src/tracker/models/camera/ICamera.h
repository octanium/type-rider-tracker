//
//  ICamera.h
//  typerider
//
//  Created by kikko on 8/29/13.
//
//

#ifndef typerider_ICamera_h
#define typerider_ICamera_h

#include "ofMain.h"

namespace octanium {
    
    class ICamera {
    public:
        virtual ~ICamera(){
        }
        
        virtual void setup() = 0;
        
        virtual bool update() = 0;
        
        virtual bool isReady() = 0;
        
        virtual ofParameterGroup & getParameters() = 0;
        
        virtual ofImage & getCurrFrame() = 0;
    };

}
#endif
