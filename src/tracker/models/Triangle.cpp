//
//  Triangle.cpp
//  typerider
//
//  Created by kikko on 9/2/13.
//
//

#include "Triangle.h"

namespace octanium {

    void Triangle::setup(){
        
        params.setName("Triangle");
        params.add(bDraw.set("Draw", false));
        params.add(numPointsTriangulableMax.set("Num pts triangulable max", 6000, 1000, 10000));
        report.set("Num Triangles", "");
    }

    const TrianglesVO& Triangle::triangulate(const ContoursVO & contoursVO){
        
        vo.triangles.clear();
        
        int numTriangles = 0;
        for (auto  pl : contoursVO.polylines) {
            if(pl.size() < 3){
                continue;
            }
            vector< vector<ofVec2f> > triangles = triangulate(pl);
            numTriangles += triangles.size();
            vo.triangles.insert(vo.triangles.end(), triangles.begin(), triangles.end());
        }
        
        report.set( ofToString(numTriangles) );
        
        return vo;
    }
    
    vector< vector<ofVec2f> > Triangle::triangulate(const ofPolyline& pl){
        
        triangles.clear();
        vertices.clear();
        
        // add points
        XYZ v;
        vector<ofPoint> pts = pl.getVertices();
        for(int i = 0; i < pts.size(); i++){
            v.x = pts[i].x;
            v.y = pts[i].y;
            v.z = pts[i].z;
            vertices.push_back(v);
        };
        
        int nv = vertices.size();
        
        // 3 extra points required by the Triangulate algorithm
        vertices.push_back(XYZ());
        vertices.push_back(XYZ());
        vertices.push_back(XYZ());
        
        //allocate space for triangle indices
        triangles.resize(3*nv);
        
        int ntri;
        
        // sort vertices along X axis
        qsort( &vertices[0], vertices.size()-3, sizeof( XYZ ), XYZCompare );
        
        // triangulate
        Triangulate( nv, &vertices[0], &triangles[0], ntri );
        
        // copy data to our return vector
        vector< vector<ofVec2f> > tris;
        
        //copy triangles
        int p1, p2, p3;
        vector<ofVec2f> t(3);
        for(int i = 0; i < ntri; i++){
            p1 = triangles[ i ].p1;
            p2 = triangles[ i ].p2;
            p3 = triangles[ i ].p3;
            t[0] = ofVec2f(vertices[p1].x, vertices[p1].y);
            t[1] = ofVec2f(vertices[p2].x, vertices[p2].y);
            t[2] = ofVec2f(vertices[p3].x, vertices[p3].y);
            if(pl.inside( getTriangleCenter(t) )){
                tris.push_back(t);
            }
        }
        
        return tris;
    }

    void Triangle::draw(){
        
        if(!bDraw) return;
        
        triangleMesh.clear();
        
        ofPushStyle();
        ofSetColor(ofColor::royalBlue);
        for (int i=0; i<vo.triangles.size(); i++) {
            triangleMesh.addIndex(i*3);
            triangleMesh.addIndex(i*3+1);
            triangleMesh.addIndex(i*3+2);
            triangleMesh.addVertex(vo.triangles[i][0]);
            triangleMesh.addVertex(vo.triangles[i][1]);
            triangleMesh.addVertex(vo.triangles[i][2]);
        }
        triangleMesh.drawWireframe();
        ofPopStyle();
    }
    
    ofVec2f Triangle::getTriangleCenter(vector<ofVec2f> t) {
        float ix = ( t[0].x + t[1].x + t[2].x) / 3;
        float iy = ( t[0].y + t[1].y + t[2].y) / 3;
        return ofVec2f(ix, iy);
    }

}