//
//  Triangle.h
//  typerider
//
//  Created by kikko on 9/2/13.
//
//

#ifndef __typerider__Triangle__
#define __typerider__Triangle__

#include "ofMain.h"
#include "DataTypes.h"
#include "Delaunay.h"

namespace octanium {
    
    class Triangle {
        
    public:
        Triangle(){}
        virtual ~Triangle(){}
        
        void setup();
        const TrianglesVO& triangulate(const ContoursVO & contoursVO);
        void draw();
        
        // target number of triangles
        ofParameterGroup params;
        ofParameter<int> numPointsTriangulableMax;
        ofParameter<bool> bDraw;
        ofParameter<string> report;
        
    private:
        
        vector< vector<ofVec2f> > triangulate(const ofPolyline& pl);
        
        TrianglesVO vo;
        
        vector<ITRIANGLE> triangles;
        vector<XYZ> vertices;
        ofMesh triangleMesh;
                
        ofVec2f getTriangleCenter(vector<ofVec2f> t);
    };
}

#endif /* defined(__typerider__Triangle__) */
