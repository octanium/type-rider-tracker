//
//  Sender.cpp
//  typerider
//
//  Created by kikko on 9/2/13.
//
//

#include "Sender.h"

namespace octanium {
    
    void Sender::setup(){
        oscHost.set("OSC Host", "localhost");
        oscPort.set("OSC Port", "12345");
        numTrianglesPerMessage.set("Num triangles per message", 50, 1, 100);
    }
    
    void Sender::connect(){
        osc.setup(oscHost, ofToInt(oscPort));
    }
    
    void Sender::send(const TrianglesVO & trianglesVO){
        
        ofxOscMessage m;
        m.setAddress("/t");
        
        int i=0, j=0;
        for (auto t: trianglesVO.triangles) {
            for (j=0; j<3; j++) {
                m.addIntArg(t[j].x);
                m.addIntArg(t[j].y);
            };
            if(i++>=numTrianglesPerMessage){
                osc.sendMessage(m);
                m.clear();
                m.setAddress("/t");
                i=0;
            }
        }
        osc.sendMessage(m);
        
        m.clear();
        m.setAddress("/end");
        osc.sendMessage(m);
    }
}