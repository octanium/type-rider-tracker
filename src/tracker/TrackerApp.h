#pragma once

#include "ofMain.h"
#include "ofxGui.h"

#include "DataTypes.h"
#ifdef SIMULATION
    #include "SimulationCamera.h"
#else
    #include "ISCamera.h"
#endif
#include "Settings.h"
#include "Contours.h"
#include "Triangle.h"
#include "Sender.h"
#include "QuadWarp.h"

using namespace octanium;

/* App
 - TypeRiderTracker & TypeRiderTrackerSimulation
 - Simulation app simulate camera feed with from input image or video
 */
class TrackerApp : public ofBaseApp{

public:
    void setup();
    void update();
    void draw();
    void exit();
    void keyPressed(int key);
    
    /* MODEL : */
    Settings settings;
    ICamera * camera;
    Contours contours;
    Triangle triangle;
    Sender sender;
    
    /* VIEW : */
    QuadWarp quadWarp;
    ofxPanel gui;
    
private:
    
    void setupGui();
    void quadWarpHasChanged(QuadVO& quadVO);
    
    bool bDrawGui;
};
