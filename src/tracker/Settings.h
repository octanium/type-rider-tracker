//
//  Settings.h
//  TypeRiderTracker
//
//  Created by kikko on 9/2/13.
//
//

#ifndef __TypeRiderTracker__Settings__
#define __TypeRiderTracker__Settings__

#include "ofMain.h"
#include "Utils.h"
#include "DataTypes.h"

namespace octanium {
    
class Settings : public ofParameterGroup {
    
public:
    const Settings & setup();
    
    // Application framerate
    GlobalSetterParameter<int> framerate;
    
    // ContourFinder threshold
    ofParameter<int> threshold;
    
    // Current quad to warp input from
    ofParameter<QuadVO> quad;
    
    // videoprojector's resolution
    ofParameter<int> vpWidth;
    ofParameter<int> vpHeight;
};
    
}

#endif /* defined(__TypeRiderTracker__Settings__) */
