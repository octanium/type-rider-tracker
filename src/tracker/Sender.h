//
//  Sender.h
//  typerider
//
//  Created by kikko on 9/2/13.
//
//

#ifndef __typerider__Sender__
#define __typerider__Sender__

#include "ofMain.h"
#include "ofxOsc.h"
#include "DataTypes.h"

namespace octanium {

    class Sender {
        
    public:
        void setup();
        void connect();
        void send(const TrianglesVO & trianglesVO);
        
        ofParameter<int> numTrianglesPerMessage;
        
        // hostname or IP to send Triangles to
        ofParameter<string> oscHost;
        
        // port the host listening to
        ofParameter<string> oscPort;
        
    private:
        ofxOscSender osc;
    };
}

#endif /* defined(__typerider__Sender__) */
