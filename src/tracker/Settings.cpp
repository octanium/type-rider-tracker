//
//  Settings.cpp
//  TypeRiderTracker
//
//  Created by kikko on 9/2/13.
//
//

#include "Settings.h"

namespace octanium {

const Settings & Settings::setup(){
    
    setName("Application Settings");
    
    framerate.set("Framerate", 30, 1, 60);
    framerate.addSetter(&ofSetFrameRate);
    add( framerate );
    
    quad.setName("Quad");
    add( quad );
    
    add( vpWidth.set("VideoProj width", 1024, 1024, 1680) );
    add( vpHeight.set("VideoProj height", 768, 768, 1200) );
    
    return *this;
}

}