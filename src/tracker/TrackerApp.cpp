#include "TrackerApp.h"

void TrackerApp::setup(){
    
    ofSetVerticalSync(true);
    ofBackground(20);
    
#ifdef SIMULATION
    camera = new SimulationCamera();
#else
    camera = new ISCamera();
#endif
    camera->setup();
    settings.setup();
    contours.setup();
    triangle.setup();
    sender.setup();
    
    setupGui();
    
    sender.connect();
    
    ofSetWindowShape(camera->getCurrFrame().getWidth(), camera->getCurrFrame().getHeight());
    
    quadWarp.setup(settings.quad);
    ofAddListener(quadWarp.hasChanged, this, &TrackerApp::quadWarpHasChanged);
    
    bDrawGui = true;
}

void TrackerApp::exit(){
    if(camera){
        delete camera;
    }
}

void TrackerApp::setupGui(){
    
#ifdef SIMULATION
    string settings_filename = "defaults_simulation.xml";
#else
    string settings_filename = "defaults.xml";
#endif
    
    gui.setDefaultWidth(270);
    gui.setDefaultBorderColor(ofColor(0,0,0));
    gui.setDefaultBackgroundColor(ofColor(0,0,0));
    gui.setDefaultFillColor(ofColor::royalBlue);
    
    gui.setup("TypeRider Tracker", settings_filename);
    gui.setBorderColor(ofColor(0,0,0,0));
    gui.setBackgroundColor(ofColor(0,0,0,0));
    
    gui.add(camera->getParameters());
    gui.add(settings);
    gui.add(sender.oscHost);
    gui.add(sender.oscPort);
    gui.add(sender.numTrianglesPerMessage);
    gui.add(contours.params);
    gui.add(contours.report);
    gui.add(triangle.params);
    gui.add(triangle.report);
    
    gui.loadFromFile(settings_filename);
}

/* get quadVO from quadView */
void TrackerApp::quadWarpHasChanged(QuadVO& quadVO){
    settings.quad.set(quadVO);
}

void TrackerApp::update(){
    
    /* Retrieve frame from camera
     - ISCamera provides ofParameters for : bUndistord, gain, exposure, shutter, gamma
     */
    if(camera->update()) {
        
        /* Contours gives masked & unwarped blobs contours from quad to videoproj resolution
         - provides ofParameters for : threshold
         */
        const ContoursVO & contoursVO = contours.find(camera->getCurrFrame(), settings.quad, settings.vpWidth, settings.vpHeight);
        
        if(contoursVO.numTotalPoints < triangle.numPointsTriangulableMax && ofGetElapsedTimef()>3) {
            /* Triangle returns a triangulation from contours
             - provides ofParameter for a target number of triangles
             */
            const TrianglesVO & trianglesVO = triangle.triangulate(contoursVO);
            
            /* Serialize and send triangles via OSC */
            sender.send(trianglesVO);
        }
    }
    
    quadWarp.update();
}

void TrackerApp::draw(){
    
    camera->getCurrFrame().draw(0, 0);
    
    quadWarp.draw();
    contours.draw();
    triangle.draw();
    
    if(bDrawGui){
        gui.draw();
    }
    if(!camera->isReady()){;
        ofDrawBitmapString("WAITING FOR CAMERA...", ofGetWidth()*0.5-80, ofGetHeight()*0.5-20);
    }
    
    ofSetWindowTitle(ofToString(ofGetFrameRate(),1)+"fps");
}

void TrackerApp::keyPressed(int key){
    if(key=='\t'){
        bDrawGui = !bDrawGui;
    } else if (key=='f'){
        ofToggleFullscreen();
    }
}
